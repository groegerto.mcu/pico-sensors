# MP503

An analog gas sensor for measuring volatile organic compounds (VOC), especially
alcohol, smoke, iso-butane and methanal.

A suitable load resistor for measuring is 33kOhms.