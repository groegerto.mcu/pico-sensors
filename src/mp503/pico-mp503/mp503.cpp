#include "mp503.h"


namespace pico_sensors
{

    constexpr picopp::Duration WARM_UP_TIME = picopp::Duration::s(5);

    MP503::MP503(uint analogPin, uint heaterPin) :
      adc(analogPin), heater(heaterPin, picopp::GPIOPin::Direction::DIR_OUT)
    {
        heater << false;
    }

    uint16_t
    MP503::sample()
    {
        return adc.read();
    }

    void
    MP503::enableHeater(bool enable)
    {
        heater.put(enable);
    }


} // namespace pico_sensors
