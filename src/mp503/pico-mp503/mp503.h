#pragma once

#include <optional>

#include <picopp/gpio/GPIO.h>
#include <picopp/hardware/ADC.h>
#include <picopp/time/Timestamp.h>

namespace pico_sensors
{

    /*!
     * \brief An abstraction of the MP503 analog VOC sensor.
     *
     * This class uses an adc pin.
     * The result from the sensor is a 12 bit analog value.
     * The heater should be switched with a transistor as the gpios
     * can't drive enough current for the heater.
     *
     * The sensor should be used in a configuration like this:
     * A suitable value for R_l is 33kOhms.
     *
     *  V_dd >-------|
     *               |
     *       sensor electrodes
     *               |
     *  GND >---R_l--|
     *               |
     *  analog pin >-|
     */
    class MP503
    {
      public:
        /*!
         * \brief Create a sensor with the given configuration.
         * \param analogPin the analog pin used (gpio pin number, not adc pin number)
         * \param heaterPin the heater pin used
         */
        MP503(uint analogPin, uint heaterPin);

        MP503(MP503&) = delete;
        MP503(MP503&&) = default;

        /*!
         * \brief Read an analog value from the sensor.
         * \return the 12 bit analog sensor value
         *
         * A higher value indicates a higher concentration of the target gases.
         */
        uint16_t sample();

        /*!
         * \brief Enable the onboard heater.
         * \param enable true to enable the heater, false otherwise
         */
        void enableHeater(bool enable);

      private:
        picopp::ADCPin adc;
        picopp::GPIOPin heater;
    };

} // namespace pico_sensors
