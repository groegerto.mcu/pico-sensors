#pragma once

#include <functional>
#include <optional>

#include <hardware/uart.h>
#include <picopp/gpio/GPIO.h>
#include <picopp/time/Timestamp.h>
#include <picopp/util/result.h>

namespace pico_sensors
{

    /*!
     * \brief An abstraction of the PMS7003 PM sensor.
     *
     * This class uses a hardware uart instance.
     */
    class PMS7003
    {
      public:
        /*!
         * \brief The sensor data.
         */
        struct Data {
            uint16_t pm1_0_CF; ///< PM1.0 concentration (CF=1, standard particle) [ug/m^3]
            uint16_t pm2_5_CF; ///< PM2.5 concentration (CF=1, standard particle) [ug/m^3]
            uint16_t pm10_CF;  ///< PM10. concentration (CF=1, standard particle) [ug/m^3]
            uint16_t pm1_0;    ///< PM1.0 concentration (under atmospheric environment) [ug/m^3]
            uint16_t pm2_5;    ///< PM2.5 concentration (under atmospheric environment) [ug/m^3]
            uint16_t pm10;     ///< PM10. concentration (under atmospheric environment) [ug/m^3]

            uint16_t p0_3;  /// < Particles > 0.3 microns [/0.1L]
            uint16_t p0_5;  /// < Particles > 0.5 microns [/0.1L]
            uint16_t p1_0;  /// < Particles > 1.0 microns [/0.1L]
            uint16_t p2_5;  /// < Particles > 2.5 microns [/0.1L]
            uint16_t p5_0;  /// < Particles > 5.0 microns [/0.1L]
            uint16_t p10_0; /// < Particles > 10. microns [/0.1L]
        };
        using DataCallback = std::function<void(Data)>;

        /*!
         * \brief Error codes when using the sensor.
         */
        enum ErrorCode {
            OK,
            TIMEOUT,          ///< timeout
            NO_ADDRESS,       ///< no valid address found
            INVALID_CHECKSUM, ///< invalid checksum of data
            ERROR,            ///< generic error during measurement
        };


        /*!
         * \brief Create a sensor with the given configuration.
         * \param uart the uart instance to use
         * \param rxPin the rx pin
         * \param txPin the tx pin
         * \param resetPin the reset pin
         *
         * Initialization of the sensor might take up to ~3 seconds.
         * The set pin of the sensor is not used as the same functionality can be achieved with
         * the sleep command.
         */
        PMS7003(uart_inst_t* uart, uint rxPin, uint txPin, uint resetPin);

        PMS7003(PMS7003&& other);
        ~PMS7003();

        /*!
         * \brief Register a callback to be used when sensor data is received during active mode.
         * \param dataCallback the callback to use
         */
        void registerCallback(const DataCallback& dataCallback);

        /*!
         * \brief Set active/passive mode.
         * \param active true for active mode, false for passive
         *
         * During active mode sensor data samples are sent automatically to the registered callback
         * by \ref registerCallback.
         * When passive mode as used every sample has to be requested with \ref readSample.
         */
        void setMode(bool active);
        /*!
         * \brief Enable/disable sleep mode.
         * \param sleep true for sleep mode, false for waking up
         */
        void enableSleepMode(bool sleep);

        /*!
         * \brief Read a sample, works only during passive mode.
         * \return the sensor data, or an error code
         */
        picopp::Result<Data, ErrorCode> readSample();

        /*!
         * \brief Do a hardware reset.
         *
         * Only works if the reset pin was set.
         */
        void hwReset();

        /*!
         * \brief Loop function to be called in the main application loop.
         */
        void loop();

      private:
        bool skipGarbageTimeout(const picopp::Timestamp& timeout);
        ErrorCode readRaw(Data* data);
        void loadDataFromBuffer(Data* data, const uint8_t* buffer);
        uint16_t calcLrc(const uint8_t* data, uint16_t length);

        void sendCommand(const uint8_t* cmd);

      private:
        uart_inst_t* uart;
        std::optional<picopp::GPIOPin> resetPin = std::nullopt;
        DataCallback callback;
    };

} // namespace pico_sensors
