#include "pms7003.h"

#include <pico-sensor-utils/utils.h>

namespace pico_sensors
{

    constexpr uint BAUDRATE = 9'600;

    constexpr uint8_t ADDRESS[] = { 0x42, 0x4d };
    constexpr uint16_t ADDRESS_LRC = 0x8f;

    constexpr uint8_t CMD_READ[] = { 0xe2, 0, 0 };
    constexpr uint8_t CMD_MODE_ACTIVE[] = { 0xe1, 0, 1 };
    constexpr uint8_t CMD_MODE_PASSIVE[] = { 0xe1, 0, 0 };
    constexpr uint8_t CMD_SLEEP[] = { 0xe4, 0, 0 };
    constexpr uint8_t CMD_WAKEUP[] = { 0xe4, 0, 1 };

    constexpr picopp::Duration UART_TIMEOUT = picopp::Duration::ms(500);


    PMS7003::PMS7003(uart_inst_t* uart, uint rxPin, uint txPin, uint resetPin) : uart(uart)
    {
        if (resetPin >= 0) {
            this->resetPin = picopp::GPIOPin(resetPin, picopp::GPIOPin::Direction::DIR_OUT);
            *this->resetPin << false;
        }

        uart_init(uart, BAUDRATE);
        uart_set_format(uart, 8, 1, UART_PARITY_NONE);
        uart_set_translate_crlf(uart, false);

        gpio_set_function(rxPin, GPIO_FUNC_UART);
        gpio_set_function(txPin, GPIO_FUNC_UART);

        hwReset();
    }

    PMS7003::PMS7003(PMS7003&& other) : uart(other.uart), resetPin(other.resetPin)
    {
        other.uart = nullptr;
    }

    PMS7003::~PMS7003()
    {
        if (uart != nullptr) {
            uart_deinit(uart);
        }
    }


    void
    PMS7003::registerCallback(const DataCallback& dataCallback)
    {
        callback = dataCallback;
    }

    void
    PMS7003::setMode(bool active)
    {
        if (active) {
            sendCommand(CMD_MODE_ACTIVE);
        } else {
            sendCommand(CMD_MODE_PASSIVE);
        }
        drainUartRxQueue(uart);
    }

    void
    PMS7003::enableSleepMode(bool sleep)
    {
        if (sleep) {
            sendCommand(CMD_SLEEP);
        } else {
            sendCommand(CMD_WAKEUP);
        }
        drainUartRxQueue(uart);
    }

    picopp::Result<PMS7003::Data, PMS7003::ErrorCode>
    PMS7003::readSample()
    {
        sendCommand(CMD_READ);
        Data data;
        auto res = readRaw(&data);
        if (res != OK) {
            return picopp::Error(res);
        }
        return data;
    }

    void
    PMS7003::hwReset()
    {
        if (resetPin) {
            *resetPin << false;
            sleep_ms(50);
            *resetPin << true;
            sleep_ms(2000);
        }
    }


    void
    PMS7003::loop()
    {
        // the queue on the uart peripheral fits a whole sensor sample,
        // so a simple polling style works fine
        if (callback && uart_is_readable(uart)) {
            // found readable
            Data data;
            auto res = readRaw(&data);
            if (res == OK) {
                callback(data);
            } else {
                printf("[[PMS7003]]: Error! received invalid data during loop %d\n", (int) res);
            }
        }
    }


    bool
    PMS7003::skipGarbageTimeout(const picopp::Timestamp& timeout)
    {
        while (!timeout.reached()) {
            if (uart_is_readable(uart) && uart_getc(uart) == ADDRESS[0]) {
                return false;
            }
        }
        return true;
    }


    PMS7003::ErrorCode
    PMS7003::readRaw(Data* data)
    {
        picopp::Timestamp timeout = picopp::Timestamp::in(UART_TIMEOUT);

        // this will ensure first address byte is received
        if (skipGarbageTimeout(timeout)) {
            return TIMEOUT;
        }

        uint8_t buffer[31];
        if (readUartTimeout(uart, buffer, sizeof(buffer), timeout) != 31) {
            return TIMEOUT;
        }

        if (buffer[0] != ADDRESS[1]) {
            return NO_ADDRESS;
        }

        // the received frame length does not match our sensor
        if (buffer[1] != 0 || buffer[2] != 0x1c) {
            return ERROR;
        }

        uint16_t lrc = calcLrc(buffer, 29) + ADDRESS[0]; // first byte not in original buffer
        uint16_t receivedLrc = buffer[29] << 8 | buffer[30];
        if (lrc != receivedLrc) {
            return INVALID_CHECKSUM;
        }

        loadDataFromBuffer(data, buffer + 3);

        return OK;
    }

    void
    PMS7003::loadDataFromBuffer(Data* data, const uint8_t* buffer)
    {
        data->pm1_0_CF = buffer[0] << 8 | buffer[1];
        data->pm2_5_CF = buffer[2] << 8 | buffer[3];
        data->pm10_CF = buffer[4] << 8 | buffer[5];
        data->pm1_0 = buffer[6] << 8 | buffer[7];
        data->pm2_5 = buffer[8] << 8 | buffer[9];
        data->pm10 = buffer[10] << 8 | buffer[11];
        data->p0_3 = buffer[12] << 8 | buffer[13];
        data->p0_5 = buffer[14] << 8 | buffer[15];
        data->p1_0 = buffer[16] << 8 | buffer[17];
        data->p2_5 = buffer[18] << 8 | buffer[19];
        data->p5_0 = buffer[20] << 8 | buffer[21];
        data->p10_0 = buffer[22] << 8 | buffer[23];
    }

    uint16_t
    PMS7003::calcLrc(const uint8_t* data, uint16_t length)
    {
        uint16_t sum = 0;
        for (uint16_t i = 0; i < length; i++) {
            sum += data[i];
        }
        return sum;
    }

    void
    PMS7003::sendCommand(const uint8_t* cmd)
    {
        const uint16_t lrc = calcLrc(cmd, 3) + ADDRESS_LRC;
        uint8_t lrcBuf[] = { (uint8_t) (lrc >> 8), (uint8_t) lrc };

        uart_write_blocking(uart, ADDRESS, 2);
        uart_write_blocking(uart, cmd, 3);
        uart_write_blocking(uart, lrcBuf, 2);
    }


} // namespace pico_sensors
