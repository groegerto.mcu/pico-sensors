#include "utils.h"

namespace pico_sensors
{

    uint32_t
    readUartTimeout(uart_inst_t* uart,
                    uint8_t* buffer,
                    uint32_t size,
                    const picopp::Timestamp& timeout)
    {
        uint32_t i = 0;
        while (!timeout.reached() && i < size) {
            if (uart_is_readable(uart)) {
                buffer[i++] = uart_getc(uart);
            }
        }
        return i;
    }

    void
    drainUartRxQueue(uart_inst_t* uart)
    {
        sleep_ms(20);
        while (uart_is_readable(uart)) {
            uart_getc(uart);
            sleep_ms(1);
        }
    }

} // namespace pico_sensors
