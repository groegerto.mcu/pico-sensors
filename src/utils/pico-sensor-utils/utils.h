#pragma once

#include <hardware/uart.h>
#include <picopp/time/Timestamp.h>

namespace pico_sensors
{

    /*!
     * \brief Attempt to read given number of bytes from uart but stop at the given timeout.
     * \param uart the uart instance to use
     * \param buffer the buffer to write into, must accept at least size bytes
     * \param size the number of bytes to read
     * \param timeout the timeout when to abort the read
     * \return the number of bytes read
     */
    uint32_t readUartTimeout(uart_inst_t* uart,
                             uint8_t* buffer,
                             uint32_t size,
                             const picopp::Timestamp& timeout);

    /*!
     * \brief Drain the uart receive queue.
     * \param uart the uart instance to use
     */
    void drainUartRxQueue(uart_inst_t* uart);

} // namespace pico_sensors
