#pragma once

#include <hardware/uart.h>
#include <picopp/util/result.h>

namespace pico_sensors
{

    /*!
     * \brief An abstraction of the Senseair S8 CO2 sensor.
     *
     * This class uses a hardware uart instance.
     */
    class SenseairS8
    {
      public:
        /*!
         * \brief Error codes when using the sensor.
         */
        enum ErrorCode {
            OK,
            TIMEOUT,                  ///< timeout during communication
            INVALID_RESPONSE_ADDRESS, ///< invalid response address
            INVALID_RESPONSE,         ///< invalid response format
            INVALID_CHECKSUM,         ///< invalid checksum of data

            EXC_ILLEGAL_FUNCTION,     ///< function isn't implemented
            EXC_ILLEGAL_DATA_ADDRESS, ///< register is not assigned
            EXC_ILLEGAL_DATA_VALUE,   ///< invalid quantity of registers

            SENSOR_ERROR,      ///< sensor has an internal error
            CALIBRATION_ERROR, ///< error during calibration

            OUT_OF_RANGE, ///< Measured sensor value is out of range and likely an error with ABC calibration

            ERROR, ///< generic error during measurement
        };

        /*!
         * \brief The type of calibration to use
         */
        enum class CalibrationType {
            FreshAir, ///< Fresh air calibration, assumes 400ppm CO2 sensor exposure
            Zero      ///< Nitrogen calibration, assumes 0ppm CO2 sensor exposure
        };

        /*!
         * \brief A sample of the sensor including error status
         */
        struct Sample {
            uint16_t co2;  ///< the co2 value
            uint8_t error; ///< the error status
        };

        /*! \brief Can be used to address any sensor without knowing the individual address. */
        static constexpr uint8_t ANY_ADDRESS = 0xfe;

        // meter status masks
        /*! \brief fatal error, try to restart sensor */
        static constexpr uint16_t ERROR_MASK_FATAL_ERROR = 0x01;
        /*! \brief recovery procedure */
        static constexpr uint16_t ERROR_MASK_OFFSET_REGULATION = 0x02;
        /*! \brief wrong configuration, check sensor settings */
        static constexpr uint16_t ERROR_MASK_ALGORITHM_ERROR = 0x04;
        /*! \brief output error, check loads of output */
        static constexpr uint16_t ERROR_MASK_OUTPUT_ERROR = 0x08;
        /*! \brief self diagnostic error, try calibration */
        static constexpr uint16_t ERROR_MASK_SELF_DIAG_ERROR = 0x10;
        /*! \brief out of range error, resets automatically */
        static constexpr uint16_t ERROR_MASK_OUT_OF_RANGE = 0x20;
        /*! \brief memory error */
        static constexpr uint16_t ERROR_MASK_MEMORY_ERROR = 0x40;
        /*! \brief any error in status code */
        static constexpr uint16_t ERROR_MASK_ANY_ERROR = 0x7f;


        /*!
         * \brief Create a sensor with the given configuration.
         * \param uart the uart hardware to use
         * \param addr the address of the individual sensor
         * \param rxPin the receive pin
         * \param txPin the transmit pin
         */
        SenseairS8(uart_inst_t* uart, uint8_t addr, uint rxPin, uint txPin);

        SenseairS8(SenseairS8&& other);
        ~SenseairS8();


        /*!
         * \brief Read the internal error status of the sensor.
         * \return the status or an error code
         *
         * The returned status can be checked with the ERROR_MASK_ masks.
         */
        picopp::Result<uint8_t, ErrorCode> readErrorStatus();

        /*!
         * \brief Read whether the alarm output is enabled.
         * \return true when the alarm is enabled, false otherwise or an error code
         */
        picopp::Result<bool, ErrorCode> readAlarmStatus();
        /*!
         * \brief Read whether the pwm output is fully enabled
         * \return true when the pwm output is at max value, false otherwise, or an error code
         */
        picopp::Result<bool, ErrorCode> readPWMStatus();
        /*!
         * \brief Read the current CO2 value in ppm.
         * \return the CO2 ppm value or an error code
         */
        picopp::Result<uint16_t, ErrorCode> readCO2();
        /*!
         * \brief Read the current pwm output, 0x3fff represents max output
         * \return the pwm value or an error code
         */
        picopp::Result<uint16_t, ErrorCode> readPWM();

        /*!
         * \brief Read the 3-byte sensor type id.
         * \return the type id or an error code
         */
        picopp::Result<uint32_t, ErrorCode> readSensorTypeID();
        /*!
         * \brief Read memory map version.
         * \return the version or an error code.
         */
        picopp::Result<uint16_t, ErrorCode> readMemoryMapVersion();
        /*!
         * \brief Read the firmware version, high byte is main revision, low byte is sub revision.
         * \return the firmware version or an error code
         */
        picopp::Result<uint16_t, ErrorCode> readFWVersion();
        /*!
         * \brief Read the 4-byte sensor's serial number.
         * \return the sensor serial number or an error code
         */
        picopp::Result<uint32_t, ErrorCode> readSensorID();

        /*!
         * \brief Perform sensor's background calibration with the given type.
         * \param type the type of calibration to do
         * \return an error code \ref ErrorCode::OK for success
         */
        ErrorCode performBackgroundCalibration(CalibrationType type);

        /*!
         * \brief Read the current ABC algorithm period in hours.
         * \return the ABC period in hours or an error code
         */
        picopp::Result<uint16_t, ErrorCode> readABCPeriod();
        /*!
         * \brief Set the ABC period in hours. A value of 0 disables ABC algorithm.
         * \param abcPeriod the ABC period in hours
         * \return an error code \ref ErrorCode::OK for success
         *
         * The ABC algorithm is used to self correct the sensor to the lowest reading of
         * the sensor in the given ABC period to correct for long-term drift.
         */
        ErrorCode setABCPeriod(uint16_t abcPeriod);

        /*!
         * \brief Read the internal sensor status and CO2 value.
         * \return a sensor sample or an error code
         */
        picopp::Result<Sample, ErrorCode> sample();

      public:
        ErrorCode readRegisters(uint8_t cmd, uint8_t reg, uint8_t number, uint16_t* target);
        ErrorCode writeRegister(uint8_t reg, uint16_t value);

        void writeUart(const uint8_t* data, uint32_t length);
        ErrorCode mapException(uint8_t exceptionCode);
        uint16_t mapCalibrationCommand(CalibrationType type);
        uint16_t mapCalibrationAck(CalibrationType type);

        uint16_t calcCRC(const uint8_t* data, uint32_t length, uint16_t start = 0xffff);

      private:
        uart_inst_t* uart;
        uint8_t address;
    };

} // namespace pico_sensors
