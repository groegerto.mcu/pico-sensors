#include "senseair-s8.h"

#include <pico-sensor-utils/utils.h>

namespace pico_sensors
{

    constexpr uint BAUDRATE = 9'600;

    constexpr uint16_t CRC16_IBM = 0xA001;

    // commands
    constexpr uint8_t CMD_READ_HOLDING_REGISTER = 0x03;
    constexpr uint8_t CMD_READ_INPUT_REGISTER = 0x04;
    constexpr uint8_t CMD_WRITE_REGISTER = 0x06;

    // input registers
    constexpr uint8_t IR_MeterStatus = 0;
    constexpr uint8_t IR_OutputStatus = 2;
    constexpr uint8_t IR_CO2 = 3;
    constexpr uint8_t IR_PWM = 21;

    constexpr uint8_t IR_SensorTypeIDHigh = 25;
    constexpr uint8_t IR_SensorTypeIDLow = 26;
    constexpr uint8_t IR_MemoryMapVersion = 27;
    constexpr uint8_t IR_FWVersion = 28;
    constexpr uint8_t IR_SensorIDHigh = 29;
    constexpr uint8_t IR_SensorIDLow = 30;

    // holding registers
    constexpr uint8_t HR_Acknowledgment = 0;
    constexpr uint8_t HR_Command = 1;
    constexpr uint8_t HR_ABCPeriod = 31;


    // output status masks
    constexpr uint16_t MASK_OUTPUT_ALARM = 0x01;
    constexpr uint16_t MASK_OUTPUT_PWM = 0x02;

    // acknowledgement masks
    constexpr uint16_t MASK_FRESH_AIR_CALIBRATION = 0x20;
    constexpr uint16_t MASK_ZERO_CALIBRATION = 0x40;

    // command masks
    constexpr uint16_t MASK_CMD_FRESH_AIR_CALIBRATION = 0x7c06;
    constexpr uint16_t MASK_CMD_ZERO_CALIBRATION = 0x7c07;

    // exception codes
    constexpr uint8_t EXCEPTION_ILLEGAL_FUNCTION = 0x01;
    constexpr uint8_t EXCEPTION_ILLEGAL_DATA_ADDRESS = 0x02;
    constexpr uint8_t EXCEPTION_ILLEGAL_DATA_VALUE = 0x03;

    constexpr picopp::Duration UART_TIMEOUT = picopp::Duration::ms(500);


    SenseairS8::SenseairS8(uart_inst_t* uart, uint8_t addr, uint rxPin, uint txPin) :
      uart(uart), address(addr)
    {
        uart_init(uart, BAUDRATE);
        uart_set_format(uart, 8, 1, UART_PARITY_NONE);
        uart_set_translate_crlf(uart, false);

        gpio_set_function(rxPin, GPIO_FUNC_UART);
        gpio_set_function(txPin, GPIO_FUNC_UART);

        // dummy write to setup lines
        uint8_t buf = 0;
        uart_write_blocking(uart, &buf, 1);

        drainUartRxQueue(uart);
    }

    SenseairS8::SenseairS8(SenseairS8&& other) : uart(other.uart), address(other.address)
    {
        other.uart = nullptr;
    }

    SenseairS8::~SenseairS8()
    {
        if (uart != nullptr) {
            uart_deinit(uart);
        }
    }


    picopp::Result<uint8_t, SenseairS8::ErrorCode>
    SenseairS8::readErrorStatus()
    {
        uint16_t error;
        auto res = readRegisters(CMD_READ_INPUT_REGISTER, IR_MeterStatus, 1, &error);
        if (res == OK) {
            return (uint8_t) error;
        }
        return picopp::Error(res);
    }

    picopp::Result<bool, SenseairS8::ErrorCode>
    SenseairS8::readAlarmStatus()
    {
        uint16_t status;
        auto res = readRegisters(CMD_READ_INPUT_REGISTER, IR_OutputStatus, 1, &status);
        if (res == OK) {
            return !(status & MASK_OUTPUT_ALARM);
        }
        return picopp::Error(res);
    }

    picopp::Result<bool, SenseairS8::ErrorCode>
    SenseairS8::readPWMStatus()
    {
        uint16_t status;
        auto res = readRegisters(CMD_READ_INPUT_REGISTER, IR_OutputStatus, 1, &status);
        if (res == OK) {
            return status & MASK_OUTPUT_PWM;
        }
        return picopp::Error(res);
    }

    picopp::Result<uint16_t, SenseairS8::ErrorCode>
    SenseairS8::readCO2()
    {
        uint16_t co2;
        auto res = readRegisters(CMD_READ_INPUT_REGISTER, IR_CO2, 1, &co2);
        if (res == OK) {
            if (co2 > 10000) {
                return picopp::Error(ErrorCode::OUT_OF_RANGE);
            }
            return co2;
        }
        return picopp::Error(res);
    }

    picopp::Result<uint16_t, SenseairS8::ErrorCode>
    SenseairS8::readPWM()
    {
        uint16_t pwm;
        auto res = readRegisters(CMD_READ_INPUT_REGISTER, IR_PWM, 1, &pwm);
        if (res == OK) {
            return pwm;
        }
        return picopp::Error(res);
    }

    picopp::Result<uint32_t, SenseairS8::ErrorCode>
    SenseairS8::readSensorTypeID()
    {
        uint16_t typeHigh;
        uint16_t typeLow;
        auto res = readRegisters(CMD_READ_INPUT_REGISTER, IR_SensorTypeIDHigh, 1, &typeHigh);
        if (res == OK) {
            res = readRegisters(CMD_READ_INPUT_REGISTER, IR_SensorTypeIDLow, 1, &typeLow);
            if (res == OK) {
                return typeHigh << 16 | typeLow;
            }
        }
        return picopp::Error(res);
    }

    picopp::Result<uint16_t, SenseairS8::ErrorCode>
    SenseairS8::readMemoryMapVersion()
    {
        uint16_t version;
        auto res = readRegisters(CMD_READ_INPUT_REGISTER, IR_MemoryMapVersion, 1, &version);
        if (res == OK) {
            return version;
        }
        return picopp::Error(res);
    }

    picopp::Result<uint16_t, SenseairS8::ErrorCode>
    SenseairS8::readFWVersion()
    {
        uint16_t version;
        auto res = readRegisters(CMD_READ_INPUT_REGISTER, IR_FWVersion, 1, &version);
        if (res == OK) {
            return version;
        }
        return picopp::Error(res);
    }

    picopp::Result<uint32_t, SenseairS8::ErrorCode>
    SenseairS8::readSensorID()
    {
        uint16_t idHigh;
        uint16_t idLow;
        auto res = readRegisters(CMD_READ_INPUT_REGISTER, IR_SensorIDHigh, 1, &idHigh);
        if (res == OK) {
            res = readRegisters(CMD_READ_INPUT_REGISTER, IR_SensorIDLow, 1, &idLow);
            if (res == OK) {
                return idHigh << 16 | idLow;
            }
        }
        return picopp::Error(res);
    }

    SenseairS8::ErrorCode
    SenseairS8::performBackgroundCalibration(CalibrationType type)
    {
        // clear acknowledgement flag
        auto res = writeRegister(HR_Acknowledgment, 0);
        if (res != OK) {
            return res;
        }

        // start calibration
        res = writeRegister(HR_Command, mapCalibrationCommand(type));
        if (res != OK) {
            return res;
        }

        // wait until calibration is complete
        sleep_ms(5'000);

        // check acknowledgement flag
        uint16_t ack;
        res = readRegisters(CMD_READ_HOLDING_REGISTER, HR_Acknowledgment, 1, &ack);
        if (res != OK) {
            return res;
        }

        if (ack & mapCalibrationAck(type)) {
            return OK;
        }

        return CALIBRATION_ERROR;
    }

    picopp::Result<uint16_t, SenseairS8::ErrorCode>
    SenseairS8::readABCPeriod()
    {
        uint16_t abc;
        auto res = readRegisters(CMD_READ_HOLDING_REGISTER, HR_ABCPeriod, 1, &abc);
        if (res == OK) {
            return abc;
        }
        return picopp::Error(res);
    }

    SenseairS8::ErrorCode
    SenseairS8::setABCPeriod(uint16_t abcPeriod)
    {
        auto res = writeRegister(HR_ABCPeriod, abcPeriod);
        return res;
    }

    picopp::Result<SenseairS8::Sample, SenseairS8::ErrorCode>
    SenseairS8::sample()
    {
        uint16_t buffer[4];
        auto res = readRegisters(CMD_READ_INPUT_REGISTER, IR_MeterStatus, 4, buffer);
        if (res == OK) {
            Sample s { .co2 = buffer[3], .error = (uint8_t) buffer[0] };
            if (s.co2 > 10000) {
                return picopp::Error(ErrorCode::OUT_OF_RANGE);
            }

            return s;
        }
        return picopp::Error(res);
    }


    SenseairS8::ErrorCode
    SenseairS8::readRegisters(uint8_t cmd, uint8_t reg, uint8_t number, uint16_t* target)
    {
        picopp::Timestamp timeout = picopp::Timestamp::in(UART_TIMEOUT);

        // send request
        uint8_t data[] = { cmd, 0, reg, 0, number };
        writeUart(data, 5);
        // receive response
        uint32_t maxSize = 5 + 2 * number;
        uint8_t buffer[maxSize]; // max response size
        uint32_t s = readUartTimeout(uart, buffer, 2, timeout);
        if (s != 2) {
            return TIMEOUT;
        }
        if (buffer[0] != address) {
            return INVALID_RESPONSE_ADDRESS;
        }
        if (buffer[1] == cmd + 0x80) {
            // exception response
            s = readUartTimeout(uart, buffer + 2, 3, timeout);
            if (s != 3) {
                return TIMEOUT;
            }
            uint16_t crc = calcCRC(buffer, 3);
            if (std::memcmp(buffer + 3, &crc, 2) != 0) {
                return INVALID_CHECKSUM;
            }
            return mapException(buffer[2]);
        }
        if (buffer[1] == cmd) {
            // successful response
            uint32_t dataLeft = maxSize - 2;
            s = readUartTimeout(uart, buffer + 2, dataLeft, timeout);
            if (s != dataLeft) {
                return TIMEOUT;
            }
            if (buffer[2] != 2 * number) {
                // returned byte count is invalid
                return INVALID_RESPONSE;
            }
            uint16_t crc = calcCRC(buffer, dataLeft);
            if (std::memcmp(buffer + maxSize - 2, &crc, 2) != 0) {
                return INVALID_CHECKSUM;
            }
            uint8_t* bufferPtr = buffer + 3;
            for (int i = 0; i < number; i++) {
                *target++ = bufferPtr[0] << 8 | bufferPtr[1];
                bufferPtr += 2;
            }
            return OK;
        }

        return INVALID_RESPONSE;
    }

    SenseairS8::ErrorCode
    SenseairS8::writeRegister(uint8_t reg, uint16_t value)
    {
        picopp::Timestamp timeout = picopp::Timestamp::in(UART_TIMEOUT);

        // send request
        uint8_t data[] = { CMD_WRITE_REGISTER, 0, reg, (uint8_t) (value >> 8), (uint8_t) value };
        writeUart(data, 5);
        // receive response
        uint8_t buffer[5]; // max response size
        uint32_t s = readUartTimeout(uart, buffer, 5, timeout);
        if (s != 5) {
            return TIMEOUT;
        }
        if (buffer[0] != address) {
            return INVALID_RESPONSE_ADDRESS;
        }
        if (buffer[1] == CMD_WRITE_REGISTER + 0x80) {
            // exception response
            uint16_t crc = calcCRC(buffer, 3);
            if (std::memcmp(buffer + 3, &crc, 2) != 0) {
                return INVALID_CHECKSUM;
            }
            return mapException(buffer[2]);
        }
        if (buffer[1] == CMD_WRITE_REGISTER) {
            // upon success just check function code, drain rest of message
            drainUartRxQueue(uart);
            return OK;
        }

        return INVALID_RESPONSE;
    }


    void
    SenseairS8::writeUart(const uint8_t* data, uint32_t length)
    {
        // calculate crc from address + data
        uint16_t crc = calcCRC(data, length, calcCRC(&address, 1));
        uart_write_blocking(uart, &address, 1);
        uart_write_blocking(uart, data, length);
        uart_write_blocking(uart, (uint8_t*) &crc, 2);
    }

    SenseairS8::ErrorCode
    SenseairS8::mapException(uint8_t exceptionCode)
    {
        switch (exceptionCode) {
            case EXCEPTION_ILLEGAL_FUNCTION: return EXC_ILLEGAL_FUNCTION;
            case EXCEPTION_ILLEGAL_DATA_ADDRESS: return EXC_ILLEGAL_DATA_ADDRESS;
            case EXCEPTION_ILLEGAL_DATA_VALUE: return EXC_ILLEGAL_DATA_VALUE;
        }
        return ERROR;
    }

    uint16_t
    SenseairS8::mapCalibrationCommand(CalibrationType type)
    {
        switch (type) {
            case CalibrationType::FreshAir: return MASK_CMD_FRESH_AIR_CALIBRATION;
            case CalibrationType::Zero: return MASK_CMD_ZERO_CALIBRATION;
        }
        return 0;
    }

    uint16_t
    SenseairS8::mapCalibrationAck(CalibrationType type)
    {
        switch (type) {
            case CalibrationType::FreshAir: return MASK_FRESH_AIR_CALIBRATION;
            case CalibrationType::Zero: return MASK_ZERO_CALIBRATION;
        }
        return 0;
    }

    uint16_t
    SenseairS8::calcCRC(const uint8_t* data, uint32_t length, uint16_t start)
    {
        // modbus uses CRC16-IBM as a checksum
        uint16_t crc = start;
        for (uint32_t s = 0; s < length; s++) {
            crc ^= *data++;
            for (uint8_t i = 0; i < 8; i++) {
                bool flag = crc & 1;
                crc >>= 1;
                if (flag) {
                    crc ^= CRC16_IBM;
                }
            }
        }

        return crc;
    }

} // namespace pico_sensors
