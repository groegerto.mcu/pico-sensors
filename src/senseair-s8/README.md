# Senseair S8

An NDIR CO2 sensor.
It communicates over UART.

It can be addressed with an individual slave address or the common 0xfe ANY_ADDRESS.
To find out the individual address simply scan every 1-byte value and check for a response.