#include "sht41.h"

#include <algorithm>

#include <hardware/gpio.h>

namespace pico_sensors
{

    constexpr uint8_t I2C_ADDRESS = 0x44;

    constexpr uint16_t CRC8 = 0x31;

    // commands
    constexpr uint CMD_READ_SERIAL = 0x89;
    constexpr uint CMD_SOFT_RESET = 0x94;

    constexpr uint CMD_ACTIVATE_HEATER_200_1S = 0x39;
    constexpr uint CMD_ACTIVATE_HEATER_200_01S = 0x32;
    constexpr uint CMD_ACTIVATE_HEATER_110_1S = 0x2f;
    constexpr uint CMD_ACTIVATE_HEATER_110_01S = 0x24;
    constexpr uint CMD_ACTIVATE_HEATER_20_1S = 0x1e;
    constexpr uint CMD_ACTIVATE_HEATER_20_01S = 0x15;

    constexpr uint DefaultTimeoutUs = 500;

    constexpr picopp::Duration CMD_TIMEOUT = picopp::Duration::ms(1);


    SHT41::SHT41(i2c_inst_t* i2c, I2CSpeed baudrate, uint clkPin, uint dataPin, bool pullUp) :
      i2c(i2c)
    {
        i2c_init(i2c, static_cast<uint>(baudrate));

        gpio_set_function(clkPin, GPIO_FUNC_I2C);
        gpio_set_function(dataPin, GPIO_FUNC_I2C);

        if (pullUp) {
            gpio_pull_up(clkPin);
            gpio_pull_up(dataPin);
        }
    }

    SHT41::SHT41(SHT41&& other) : i2c(other.i2c)
    {
        other.i2c = nullptr;
    }

    SHT41::~SHT41()
    {
        if (i2c != nullptr) {
            i2c_deinit(i2c);
        }
    }


    picopp::Result<SHT41::Sample, SHT41::ErrorCode>
    SHT41::readSample(Precision precision)
    {
        ErrorCode res = sendCmd(static_cast<uint8_t>(precision));
        if (res != OK) return picopp::Error(res);

        mapCmdTimeout(precision).sleep();
        return receiveSample();
    }

    picopp::Result<SHT41::Sample, SHT41::ErrorCode>
    SHT41::enableHeater(Heater heater)
    {
        ErrorCode res = sendCmd(mapHeaterCommand(heater));
        if (res != OK) return picopp::Error(res);

        mapHeaterTimeout(heater.duration).sleep();
        return receiveSample();
    }

    SHT41::ErrorCode
    SHT41::reset()
    {
        return sendCmd(CMD_SOFT_RESET);
    }

    picopp::Result<uint32_t, SHT41::ErrorCode>
    SHT41::readSerial()
    {
        ErrorCode res = sendCmd(CMD_READ_SERIAL);
        if (res != OK) return picopp::Error(res);

        CMD_TIMEOUT.sleep();

        uint8_t buffer[6];
        int s = i2c_read_timeout_per_char_us(i2c, I2C_ADDRESS, buffer, 6, false, DefaultTimeoutUs);
        if (s != 6) return picopp::Error(TIMEOUT);
        if (!checkResponse(buffer)) return picopp::Error(INVALID_CHECKSUM);

        return buffer[0] << 24 | buffer[1] << 16 | buffer[3] << 8 | buffer[4];
    }


    SHT41::ErrorCode
    SHT41::sendCmd(uint8_t cmd)
    {
        int s = i2c_write_timeout_per_char_us(i2c, I2C_ADDRESS, &cmd, 1, false, DefaultTimeoutUs);
        return s == 1 ? OK : TIMEOUT;
    }

    picopp::Result<SHT41::Sample, SHT41::ErrorCode>
    SHT41::receiveSample()
    {
        uint8_t buffer[6];
        int s = i2c_read_timeout_per_char_us(i2c, I2C_ADDRESS, buffer, 6, false, DefaultTimeoutUs);
        if (s != 6) return picopp::Error(TIMEOUT);
        if (!checkResponse(buffer)) return picopp::Error(INVALID_CHECKSUM);

        return Sample { .temperature = calculateTemperature(buffer[0] << 8 | buffer[1]),
                        .humidity = calculateHumidity(buffer[3] << 8 | buffer[4]) };
    }

    float
    SHT41::calculateTemperature(uint16_t value)
    {
        return (-45 + 175 * ((float) value / 65535));
    }

    float
    SHT41::calculateHumidity(uint16_t value)
    {
        float rh = (-6 + 125 * ((float) value / 65535));
        return std::clamp(rh, 0.f, 100.f);
    }

    bool
    SHT41::checkResponse(uint8_t* data)
    {
        // response layout:
        //   0   1       2     3   4       5
        // | Data 1..2 | CRC | Data 3..4 | CRC |

        uint8_t crc = calcCrc(data);
        if (crc != data[2]) return false;

        crc = calcCrc(data + 3);
        return crc == data[5];
    }

    uint8_t
    SHT41::calcCrc(uint8_t* data)
    {
        // CRC-8, polynomial: 0x31
        // test data 0xbeef -> 0x92
        uint8_t crc = 0xff;

        for (uint8_t s = 0; s < 2; s++) {
            crc ^= *data++;
            for (uint8_t i = 0; i < 8; i++) {
                crc = (crc & 0x80) ? (crc << 1) ^ CRC8 : (crc << 1);
            }
        }

        return crc;
    }

    uint8_t
    SHT41::mapHeaterCommand(Heater heater)
    {
        if (heater.duration == Heater::s1) {
            switch (heater.power) {
                case Heater::mw200: return CMD_ACTIVATE_HEATER_200_1S;
                case Heater::mw110: return CMD_ACTIVATE_HEATER_110_1S;
                case Heater::mw20: return CMD_ACTIVATE_HEATER_20_1S;
            }
        } else if (heater.duration == Heater::s01) {
            switch (heater.power) {
                case Heater::mw200: return CMD_ACTIVATE_HEATER_200_01S;
                case Heater::mw110: return CMD_ACTIVATE_HEATER_110_01S;
                case Heater::mw20: return CMD_ACTIVATE_HEATER_20_01S;
            }
        }
        return 0;
    }

    picopp::Duration
    SHT41::mapHeaterTimeout(Heater::HeaterDuration duration)
    {
        if (duration == Heater::s1) {
            return picopp::Duration::ms(1100);
        } else if (duration == Heater::s01) {
            return picopp::Duration::ms(110);
        }
        return picopp::Duration();
    }

    picopp::Duration
    SHT41::mapCmdTimeout(Precision precision)
    {
        switch (precision) {
            case Precision::HighPrecision: return picopp::Duration::ms(10);
            case Precision::MediumPrecision: return picopp::Duration::ms(5);
            case Precision::LowPrecision: return picopp::Duration::ms(2);
        }
        return picopp::Duration();
    }

} // namespace pico_sensors
