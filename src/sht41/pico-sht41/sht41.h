#pragma once

#include <hardware/i2c.h>
#include <picopp/time/Timestamp.h>
#include <picopp/util/result.h>

namespace pico_sensors
{

    /*!
     * \brief An abstraction of the SHT41 temperature and relative humidity sensor.
     *
     * This class uses a hardware i2c instance.
     */
    class SHT41
    {
      public:
        /*! \brief The I2C speed to use */
        enum class I2CSpeed : uint {
            Standard = 100'000,  ///< standard mode
            Fast = 400'000,      ///< fast mode
            FastPlus = 1'000'000 ///< fast mode plus
        };

        /*!
         * \brief Error codes when using the sensor.
         */
        enum ErrorCode {
            OK,
            TIMEOUT,          ///< timeout during communication
            INVALID_CHECKSUM, ///< invalid checksum of data
        };

        /*!
         * \brief The precision to use for a measurement.
         */
        enum class Precision : uint8_t {
            HighPrecision = 0xfd,   ///< high precision measurement (high repeatability)
            MediumPrecision = 0xf6, ///< medium precision measurement (medium repeatability)
            LowPrecision = 0xe0,    ///< lowest precision measurement (low repeatability)
        };

        struct Heater {
            /*! \brief Power of heater operation */
            enum HeaterPower {
                mw200, ///< 200mW
                mw110, ///< 110mW
                mw20   ///< 20mW
            };
            /*! \brief Length of heater operation */
            enum HeaterDuration {
                s1, ///< 1 second
                s01 ///< 0.1 second
            };
            HeaterPower power;
            HeaterDuration duration;
        };

        /*! \brief Sensor sample */
        struct Sample {
            float temperature; ///< temperature [°C]
            float humidity;    ///< relative humidity [%]
        };

        /*!
         * \brief Create a sensor with the given configuration.
         * \param i2c the i2c hardware to use
         * \param baudrate the baudrate to use
         * \param clkPin the clock pin
         * \param dataPin the data pin
         * \param pullUp true to pull up the clk and data pins, false to not pull
         *
         * I2C lines have to be pulled up for proper operation, when internal pull up is not
         * used, the lines have to be pulled up externally.
         */
        SHT41(i2c_inst_t* i2c, I2CSpeed baudrate, uint clkPin, uint dataPin, bool pullUp);

        SHT41(SHT41&& other);
        ~SHT41();

        /*!
         * \brief Read a sample from the sensor.
         * \param precision the precision to use for measurement
         * \return the sample or an error code
         */
        picopp::Result<Sample, ErrorCode> readSample(Precision precision);
        /*!
         * \brief Enable the onboard heater followed by a high precision measurement.
         * \param heater the configuration for the heater
         * \return the resulting sample or an error code
         *
         * This function is blocking and will only return upon completion of the heating
         * duration or a timeout.
         */
        picopp::Result<Sample, ErrorCode> enableHeater(Heater heater);

        /*!
         * \brief Reset the sensor.
         * \return an error code \ref ErrorCode::OK for success
         */
        ErrorCode reset();

        /*!
         * \brief Read the unique serial number from the sensor.
         * \return the serial number or an error code
         */
        picopp::Result<uint32_t, ErrorCode> readSerial();

      private:
        ErrorCode sendCmd(uint8_t cmd);

        picopp::Result<Sample, ErrorCode> receiveSample();
        float calculateTemperature(uint16_t value);
        float calculateHumidity(uint16_t value);

        bool checkResponse(uint8_t* data);
        uint8_t calcCrc(uint8_t* data);
        uint8_t mapHeaterCommand(Heater heater);
        picopp::Duration mapHeaterTimeout(Heater::HeaterDuration duration);
        picopp::Duration mapCmdTimeout(Precision precision);

      private:
        i2c_inst_t* i2c;
    };

} // namespace pico_sensors
