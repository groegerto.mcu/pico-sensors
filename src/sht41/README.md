# SHT41

A digital temperature and relative humidity sensor.

High relative humidity (especially > 90% RH) will affect humidity measurements
and induce a positive RH offset.
This offset is reversible and the sensor will gradually converge back to within
accuracy specifications when exposed to recommended operating conditions.
This creep can be mitigated by heating the sensor, see the creep mitigation
document for further info.