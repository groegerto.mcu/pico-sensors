#include <pico-mp503/mp503.h>

#include <picopp/button/Button.h>

constexpr uint ANALOG_PIN = 26;
constexpr uint HEATER_PIN = 21;

constexpr uint BUTTON_PIN = 12;
constexpr uint BUTTON_PIN2 = 13;

using namespace pico_sensors;


int
main()
{
    // initialize stdio
    stdio_init_all();
    printf("Start MP503 test!\n");

    // Setup used for the test:
    // load resistor: 33kOhm, base resistor: 2.2kOhm

    MP503 sensor(ANALOG_PIN, HEATER_PIN);

    picopp::Button btn(BUTTON_PIN, picopp::GPIOPin::PULLUP);
    picopp::Button btn2(BUTTON_PIN2, picopp::GPIOPin::PULLUP);

    btn.setListener([&] { printf("Sensor value: %d\n", sensor.sample()); });


    bool heater = false;
    btn2.setListener([&] {
        heater = !heater;
        printf("Enable sensor heater: %d\n", heater);
        sensor.enableHeater(heater);
    });

    while (1) {
        picopp::ButtonBase::run();
        sleep_ms(10);
    }

    return 0;
}
