#include <pico-pms7003/pms7003.h>
#include <stdio.h>

#include <pico/stdlib.h>
#include <picopp/button/Button.h>
#include <picopp/util/util.h>

constexpr uint RX_PIN = 17;
constexpr uint TX_PIN = 16;
constexpr uint RESET_PIN = 19;

constexpr uint BUTTON_PIN = 15;
constexpr uint BUTTON_PIN2 = 14;

using namespace pico_sensors;

static void
printData(const PMS7003::Data& data)
{
    static int i = 0;
    printf("----------- %d\n", i++);
    printf("PM1.0  CF: %02d\n", data.pm1_0_CF);
    printf("PM2.5  CF: %02d\n", data.pm2_5_CF);
    printf("PM10.  CF: %02d\n", data.pm10_CF);
    printf("PM1.0    : %02d\n", data.pm1_0);
    printf("PM2.5    : %02d\n", data.pm2_5);
    printf("PM10.    : %02d\n", data.pm10);
    printf("P > 0.3  : %02d\n", data.p0_3);
    printf("P > 0.5  : %02d\n", data.p0_5);
    printf("P > 1.0  : %02d\n", data.p1_0);
    printf("P > 2.5  : %02d\n", data.p2_5);
    printf("P > 5.0  : %02d\n", data.p5_0);
    printf("P > 10.  : %02d\n", data.p10_0);
    puts("-----------");
}

int
main()
{
    // initialize stdio
    stdio_init_all();
    printf("Start PMS7003 test!\n");

    PMS7003 sensor(uart0, RX_PIN, TX_PIN, RESET_PIN);

    sensor.registerCallback([](PMS7003::Data data) { printData(data); });

    picopp::LongButton btn(BUTTON_PIN, picopp::GPIOPin::PULLUP);
    picopp::LongButton btn2(BUTTON_PIN2, picopp::GPIOPin::PULLUP);

    bool mode = false;
    bool sleep = false;
    btn.setListener([&] {
        printf("Change mode: %d\n", mode);
        if (mode)
            sensor.setMode(true);
        else
            sensor.setMode(false);
        mode = !mode;
    });

    btn.setLongListener([&] {
        printf("Enable sleep mode: %d\n", sleep);
        if (sleep)
            sensor.enableSleepMode(true);
        else
            sensor.enableSleepMode(false);
        sleep = !sleep;
    });

    btn2.setListener([&] {
        printf("Start read\n");
        auto data = sensor.readSample();
        data.match(picopp::overloaded {
          [](PMS7003::Data data) { printData(data); },
          [](PMS7003::ErrorCode code) { printf("Error reading sample: %d\n", (int) code); } });
    });

    while (1) {
        picopp::ButtonBase::run();
        sensor.loop();
        sleep_ms(10);
    }

    return 0;
}
