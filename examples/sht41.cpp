#include <pico-sht41/sht41.h>

#include <picopp/button/Button.h>

constexpr uint CLK_PIN = 19;
constexpr uint DATA_PIN = 18;

constexpr uint BUTTON_PIN = 15;
constexpr uint BUTTON_PIN2 = 14;

using namespace pico_sensors;


void
printSample(picopp::Result<SHT41::Sample, SHT41::ErrorCode> sample)
{
    if (sample.has_error()) {
        printf("Sensor sample err: %d\n", sample.error());
    } else {
        printf("Temperature: %.03f°C\n", sample.require().temperature);
        printf("Humidity:    %.03f%%\n", sample.require().humidity);
    }
}

int
main()
{
    // initialize stdio
    stdio_init_all();
    printf("Start SHT41 test!\n");

    // lines already pulled up by breakout board
    SHT41 sensor(i2c1, SHT41::I2CSpeed::FastPlus, CLK_PIN, DATA_PIN, false);

    auto serial = sensor.readSerial();
    if (serial) {
        printf("Sensor serial: %x\n", serial.require());
    } else {
        printf("Sensor serial: err %d\n", serial.error());
    }

    picopp::LongButton btn(BUTTON_PIN, picopp::GPIOPin::PULLUP);
    picopp::LongButton btn2(BUTTON_PIN2, picopp::GPIOPin::PULLUP);

    btn.setListener([&] {
        auto res = sensor.readSample(SHT41::Precision::HighPrecision);
        printSample(res);
    });

    int x = 0;
    btn.setLongListener([&] {
        x = x++ % 6;
        if (x == 0) {
            puts("Heat 200mW, 1s");
            auto res = sensor.enableHeater({ SHT41::Heater::mw200, SHT41::Heater::s1 });
            printSample(res);
        } else if (x == 1) {
            puts("Heat 200mW, 0.1s");
            auto res = sensor.enableHeater({ SHT41::Heater::mw200, SHT41::Heater::s01 });
            printSample(res);
        } else if (x == 2) {
            puts("Heat 110mW, 1s");
            auto res = sensor.enableHeater({ SHT41::Heater::mw110, SHT41::Heater::s1 });
            printSample(res);
        } else if (x == 3) {
            puts("Heat 110mW, 0.1s");
            auto res = sensor.enableHeater({ SHT41::Heater::mw110, SHT41::Heater::s01 });
            printSample(res);
        } else if (x == 4) {
            puts("Heat 20mW, 1s");
            auto res = sensor.enableHeater({ SHT41::Heater::mw20, SHT41::Heater::s1 });
            printSample(res);
        } else if (x == 5) {
            puts("Heat 20mW, 0.1s");
            auto res = sensor.enableHeater({ SHT41::Heater::mw20, SHT41::Heater::s01 });
            printSample(res);
        }
    });

    btn2.setLongListener([&] {
        printf("Reset sensor\n");
        sensor.reset();
    });

    while (1) {
        picopp::ButtonBase::run();
        sleep_ms(10);
    }

    return 0;
}
