#include <pico-senseair-s8/senseair-s8.h>

#include <picopp/button/Button.h>

constexpr uint RX_PIN = 17;
constexpr uint TX_PIN = 16;

constexpr uint BUTTON_PIN = 18;

using namespace pico_sensors;

template <typename T>
void
printResult(const picopp::Result<T, SenseairS8::ErrorCode>& result)
{
    result.match(
      picopp::overloaded { [](T value) { printf("value: %d\n", value); },
                           [](SenseairS8::ErrorCode code) { printf("error: %d\n", code); } });
}

int
main()
{
    // initialize stdio
    stdio_init_all();
    printf("Start Senseair S8 test!\n");

    SenseairS8 sensor(uart0, 0x68, RX_PIN, TX_PIN);

    // check sensor status
    auto status = sensor.readErrorStatus();
    printf("Sensor status: ");
    printResult(status);

    picopp::LongButton btn(BUTTON_PIN, picopp::GPIOPin::PULLDOWN);

    bool mode = false;
    bool sleep = false;
    btn.setListener([&] {
        auto res = sensor.sample();
        if (res.has_error()) {
            printf("Sensor sample err: %d\n", res.error());
        } else {
            printf("CO2 value:     %d\n", res.require().co2);
            printf("Sensor status: %x\n", res.require().error);
        }
    });

    btn.setLongListener([&] {
        puts("-------------");

        printf("ErrorStatus:   ");
        printResult(sensor.readErrorStatus());
        printf("AlarmStatus:   ");
        printResult(sensor.readAlarmStatus());
        printf("PWMStatus:     ");
        printResult(sensor.readPWMStatus());
        printf("CO2 value:     ");
        printResult(sensor.readCO2());
        printf("PWM value:     ");
        printResult(sensor.readPWM());
        printf("SensorTypeID:  ");
        printResult(sensor.readSensorTypeID());
        printf("MemMapVersion: ");
        printResult(sensor.readMemoryMapVersion());
        printf("FW version:    ");
        printResult(sensor.readFWVersion());
        printf("SensorID:      ");
        printResult(sensor.readSensorID());
        printf("ABC period:    ");
        printResult(sensor.readABCPeriod());

        puts("-------------");
    });


    while (1) {
        picopp::ButtonBase::run();
        sleep_ms(10);
    }

    return 0;
}
