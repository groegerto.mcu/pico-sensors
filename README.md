# Pico Sensor Collection

This is a collection of libraries to support different sensors on the Raspberry Pi Pico.

Supported sensors:

|Sensor|Usage|CMake target|
|---|---|---|
|PMS7003|PM sensor|`pico_pms7003`|
|SHT41|temperature and humidity sensor|`pico_sht41`|
|Senseair S8|C02 sensor|`pico_senseair_s8`|
|MP503|VOC sensor|`pico_mp503`|

## Requirements

These libraries have to be included from a project initialized with the [Raspberry Pi Pico SDK](https://github.com/raspberrypi/pico-sdk)
and [LibPico++](https://gitlab.com/groegerto.mcu/lib-picopp).


## Usage

To include to library include the `src` folder as a subdirectory in your project.
Then link to the `cmake` target of the sensor library to use.
Do **not** include the root folder directly.

Example `CMakeLists.txt`
```cmake
...
pico_sdk_init()

add_subdirectory(<path-to-this-directory>/src)
...
target_link_libraries(<target>
    <sensor-cmake-library>
)
...
```

## Example

Examples for every sensor can be found in the [examples/](examples/) directory.

